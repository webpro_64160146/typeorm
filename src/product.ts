import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productsRepository = AppDataSource.getRepository(Product)

    const products = await productsRepository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await productsRepository.findOneBy({id: 1})
    console.log(updateProduct)
    updateProduct.price = 80
    await productsRepository.save(updateProduct)
}).catch(error => console.log(error))
